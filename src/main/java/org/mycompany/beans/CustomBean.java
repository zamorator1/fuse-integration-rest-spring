package org.mycompany.beans;

import java.util.ArrayList;
import java.util.List;

import org.mycompany.model.PojoExample;

import com.google.gson.Gson;

/**
 * @author zamorator
 *
 *         clase que implementa metodos rest definidos en camel-context
 */
public class CustomBean {

	public String getById(Long id) {
		PojoExample pojo = new PojoExample();
		// TODO ir a fuente de datos para traer tupla por id
		pojo.setEdad(32);
		pojo.setNombre("Diego");

		// respondo pojo transformado a Json
		Gson gson = new Gson();
		return gson.toJson(pojo);
	}

	public String getAll() {
		List<PojoExample> listPojos = new ArrayList<PojoExample>();
		// TODO ir a fuente de datos para traer todos los pojoExample
		PojoExample pojo = new PojoExample();
		for (int i = 0; i < 5; i++) {
			pojo.setEdad(10 + i);
			pojo.setNombre("Diego " + i);
			listPojos.add(pojo);
		}
		Gson gson = new Gson();
		return gson.toJson(listPojos);

	}
	public String update( String pojoExample ) { 
		Gson gson = new Gson();
		PojoExample pojo = gson.fromJson(pojoExample, PojoExample.class);
		
		//TODO ir a fuente de datos para actualizar pojo por id
		System.out.println(pojo.getId());
		
		
		return gson.toJson(pojo);
	}
	
	public String create( String pojoExample ) {
		Gson gson = new Gson();
		PojoExample pojo = gson.fromJson(pojoExample, PojoExample.class);
		//TODO ir a fuente de datos para crear pojo
		return gson.toJson(pojo);		
	}
	
	public String delete(Long id ) {
		//TODO ir a fuente de datos para eliminar pojo por id
		return "true";	
	}

}
